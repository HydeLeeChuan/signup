/**
 * Created by LeeChuan on 21/10/16.
 */
var express = require("express");
var bodyParser = require('body-parser');

var app = express();

var suApp = {};

const PORT = 3000;


app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(express.static(__dirname +"/../client"));
app.use(express.static(__dirname +"/../bower_components"));

// app.get("/", function(req,res){
//     res.send();
//
// });

//TODO: Complete the post method and make sure client side has the service
app.post("/success", function (req, res) {
    console.log(req.body);
    res.json(req.body);

    // if(req.body.name == "Lee Chuan"){
    //     res.json({redirectUrl: "/thank-you.html"});
    //     } else{
    //          res.status
    //     }
});

app.listen(PORT,function (){
    console.log("Server is up at " + PORT);
});